import { createAction } from 'redux-act';
export const add = createAction('add');
export const minus = createAction('minus');
export const asyncAdd = createAction('asyncAdd');
export const asyncAdd_result = createAction('asyncAdd_result');
