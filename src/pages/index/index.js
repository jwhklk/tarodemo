import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { add, minus, asyncAdd,asyncAdd_result } from '../../actions'

import './index.css'


@connect(({ app }) => ({
  app
}), (dispatch) => ({
  add () {
    dispatch(add())
    console.log(dispatch(add()))
  },
  dec () {
    dispatch(minus())
  },
  asyncAdd_result () {
    dispatch(asyncAdd_result(1));
    console.log(dispatch(asyncAdd_result(1)))
  },

  asyncAddGet(){
    // Taro.showLoading({
    //   title: "loading",
    //   mask: true
    // })

    Taro.request({
      url: 'http://user.immortals.cn/hello',
      data: {},
      header: {'content-type': 'application/json'}
    }).then((res) =>{
      console.log(res.data);
      //  dispatch(asyncAdd(res.data.status))
      }
    );
  },
  asyncAddPost(){
    // Taro.showLoading({
    //   title: "loading",
    //   mask: true
    // })

    Taro.request({
      url: 'http://user.immortals.cn/hello',
      data: {},
      method : "POST",
      header: {'content-type': 'application/json'}
    }).then((res) =>{
      console.log(res.data);
      //  dispatch(asyncAdd(res.data.status))
      }
    );
  },
  asyncAdd () {
    const payload = {
      apiurl : "/hello",
      query : {}
    }
    dispatch(asyncAdd(payload));

  }
}))
class Index extends Component {

  config = {
    navigationBarTitleText: '首页'
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { console.log(this.props)}

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <Button className='add_btn' onClick={this.props.add}>+</Button>
        <Button className='dec_btn' onClick={this.props.dec}>-</Button>
        <Button className='dec_btn' onClick={this.props.asyncAdd}>async</Button>
        <Button className='dec_btn' onClick={this.props.asyncAddGet}>asyncAddGet</Button>
        <Button className='dec_btn' onClick={this.props.asyncAddPost}>asyncAddPost</Button>
        <Button className='dec_btn' onClick={this.props.asyncAdd_result}>asyncAdd_result</Button>
        <View><Text>{this.props.app.num}</Text></View>
        <View><Text>Hello, World</Text></View>
      </View>
    )
  }
}

export default Index
