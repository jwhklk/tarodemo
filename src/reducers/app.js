import { createReducer } from 'redux-act';
import { add,minus,asyncAdd_result } from '../actions';
// import Has from "lodash.has"; //json判断是否包含某属性
// import Maps from "lodash.map";//json数组循环处理
// import Flattendeep from "lodash.flattendeep";//json数组扁平化

// const INITIAL_STATE = {
//   num: 0
// }

// export default function counter (state = INITIAL_STATE, action) {
//   switch (action.type) {
//     case ADD:
//       return {
//         ...state,
//         num: state.num + 1
//       }
//      case MINUS:
//        return {
//          ...state,
//          num: state.num - 1
//        }
//      default:
//        return state
//   }
// }

const initial = {
    app:{
      num: 0
    },
};

const app = createReducer({
    //
    [add]:(state)=>{
        return { ...state, num: state.num + 1};
    },
    //
    [minus]:(state)=>{
      return { ...state, num: state.num - 1};
    },

    [asyncAdd_result]:(state, payload)=>{
      console.log("asyncAdd_result");
      console.log(state);
      console.log(payload);
      // const state=initial;
      // console.log(payload);
      return { ...state, num: state.num + payload};
    },

}, initial.app);

export default app;