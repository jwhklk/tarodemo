import './runtime';
import {effects} from 'redux-saga/dist/redux-saga.umd.min.js';
import { getDataList } from './getlist';
const {fork} = effects;
export default function* rootSaga() {
    try{
        yield fork(getDataList);
    }catch(e){
        console.log(e);
    }
}
