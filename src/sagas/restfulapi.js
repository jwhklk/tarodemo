import config from '../config';
// import fetchJsonp from 'fetch-jsonp';
import Taro from '@tarojs/taro';
// const appId = `${config.appId}`;
const fetchurl = config.serverurl;
// const secret = `${config.secret}`;

const restfulapi = {

    //get方式获取列表数据
    getlist_saga (userData) {
        const { query, apiurl } = userData;
        // console.log(userData);
        // console.log(query)
        // console.log(apiurl)
        // console.log(`${fetchurl}/v2/movie/in_theaters?start=5&count=20`)
        // let api = "https://api.douban.com/v2/movie/in_theaters?start=1&count=10";
        //IsMain, ParentId, IsShow, Grade
        
        return Taro.request({
            method: 'POST',
            url: `${fetchurl}${apiurl}`,
            data: JSON.stringify({...query}),
            header: {'content-type': 'application/json'}
        }).then((response)=>{
            let loadmoreDemo = document.getElementsByClassName('loadmore')[0];
            let loadmoreEnd = document.getElementsByClassName('loadend')[0];
            //删除加载更多
            if(!!loadmoreDemo){
                let parentNode = loadmoreDemo.parentNode;
                parentNode.removeChild(loadmoreDemo);
                //没有更多数据了
                if(response.data.data.rows.length===0){
                    let loadend = document.createElement("div");
                    loadend.className = "loadend";
                    loadend.innerHTML = "<div>- 没有更多数据了 -</div>"
                    parentNode.appendChild(loadend);
                }
            }
            //
            if(!!loadmoreEnd && query.page===1){
                let parentNode = loadmoreEnd.parentNode;
                parentNode.removeChild(loadmoreEnd);
            }
            return response.data;
        });
    },

    getlist_saga_get (userData) {
        const { query, apiurl } = userData;
        // console.log(userData);
        // console.log(query)
        // console.log(apiurl)
        // console.log(`${fetchurl}/v2/movie/in_theaters?start=5&count=20`)
        // let api = "https://api.douban.com/v2/movie/in_theaters?start=1&count=10";
        //IsMain, ParentId, IsShow, Grade
        return Taro.request({
            method: 'GET',
            url: `${fetchurl}${apiurl}`,
            data: JSON.stringify({...query}),
            header: {'content-type': 'application/json'}
        }).then((response)=>{return response.data;});
    },

    // https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe5809c42e6c00d22&redirect_uri=http://dingcanphp.applinzi.com/getUserInfo.php&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect

    
};

export default restfulapi;