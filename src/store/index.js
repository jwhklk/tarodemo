import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga/dist/redux-saga.umd.min.js';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';
import rootSaga from '../sagas';
const sagaMiddleware = createSagaMiddleware();
// let middlewares = [sagaMiddleware,thunkMiddleware];
// if (process.env.NODE_ENV === 'development') {
//   middlewares.push(require('redux-logger').createLogger())
// }
export default function configStore () {
  const store = createStore(rootReducer, applyMiddleware(sagaMiddleware,thunkMiddleware));
  // then run the saga
  sagaMiddleware.run(rootSaga);
  return store
}

// import { createStore, applyMiddleware, compose } from 'redux';
// import createSagaMiddleware from 'redux-saga/dist/redux-saga.umd.min.js';
// import thunk from 'redux-thunk';
// import { routerMiddleware } from 'react-router-redux';
// import reducer from '../reducers';
// import createHistory from 'history/createHashHistory';
// const history = createHistory();
// // import rootSaga from '../sagas';

// const middleware = routerMiddleware(history);

// let initialState = {};
// const sagaMiddleware = createSagaMiddleware();
// let configureStore = (initialState)=> {

//     const store = createStore(
//         reducer, initialState,
//         compose(
//             applyMiddleware(thunk,middleware,sagaMiddleware)
//         )
//     );

//     return store;
// }

// const store = configureStore(initialState);
// // sagaMiddleware.run(rootSaga);

// export {sagaMiddleware,history};
// export default store;
